package com.example.shuo.registerdemo


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_credential.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class CredentialFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_credential, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        credential_next_button.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("username", name_text.text.toString())
            bundle.putString("password", password_text.text.toString())
            view.findNavController().navigate(R.id.action_credentialFragment_to_ageFragment, bundle)
        }
        credential_cancel_button.setOnClickListener {
            view.findNavController().navigate(R.id.action_global_welcomeFragment)
        }
    }
}
