package com.example.shuo.registerdemo


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_age.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AgeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_age, container, false)
    }

    var dob = ""
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        dob_calendarView.setOnDateChangeListener { calendarView, year, month, dayOfMonth ->
            dob = "$month/$dayOfMonth/$year"
        }
        complete_button.setOnClickListener {
            val user = User()
            user.username = arguments?.getString("username").toString()
            user.password = arguments?.getString("password").toString()
            user.dob = dob
            (activity as MainActivity).user = user
            findNavController().navigate(R.id.action_ageFragment_to_profileFragment)
        }
        information_cancel_button.setOnClickListener {
            findNavController().navigate(R.id.action_global_welcomeFragment)
        }
    }

//    lateinit var clickLambda: (user: User) -> Unit
}
